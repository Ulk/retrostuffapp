﻿using CsvHelper.Configuration.Attributes;

namespace TestsRetroChiasse.Models
{
    public class PurchaseModel : BaseModel
    {
        [Name("date")]
        public string FormattedDate { get; set; } = "date";
        [Name("price")]
        public string Price { get; set; } = "price";
        [Name("buying_place")]
        public string BuyingPlace { get; set; } = "buying_place";
        [Name("seller")]
        public string Seller { get; set; } = "seller";

        public PurchaseModel() { }

        public PurchaseModel(BaseModel baseModel)
        {
            IdCustom = baseModel.IdCustom;
            Observations = baseModel.Observations;
            Pictures = baseModel.Pictures;
        }
    }
}
