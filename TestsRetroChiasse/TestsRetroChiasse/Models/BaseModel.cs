﻿using CsvHelper.Configuration.Attributes;

namespace TestsRetroChiasse.Models
{
    public class BaseModel
    {
        [Name("id")]
        public string IdCustom { get; set; } = "id";
        [Name("obs")]
        public string Observations { get; set; } = "obs";
        [Name("pictures")]
        public string Pictures { get; set; } = "pictures";

        //protected static string GetNameAttributeValueForProperty<T>(string propertyName)
        //{
        //    foreach (var attribute in typeof(T).GetProperty(propertyName).GetCustomAttributes(true))
        //    {
        //        if (attribute is NameAttribute nameAttribute)
        //        {
        //            return nameAttribute.Names[0];
        //        }
        //    }
        //    return null;
        //}
    }
}
