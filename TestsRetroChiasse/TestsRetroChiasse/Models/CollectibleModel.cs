﻿using CsvHelper.Configuration.Attributes;

namespace TestsRetroChiasse.Models
{
    public class CollectibleModel : BaseModel
    {
        [Name("purchase_id")]
        public string PurchaseId{ get; set; } = "purchase_id";
        [Name("price")]
        public string Price { get; set; } = "price";
        [Name("type")]
        public string Type { get; set; } = "type";
        [Name("format")]
        public string Format{ get; set; } = "format";
        [Name("object")]
        public string Object{ get; set; } = "object";

        public CollectibleModel() { }

        public CollectibleModel(BaseModel baseModel)
        {
            IdCustom = baseModel.IdCustom;
            Observations = baseModel.Observations;
            Pictures = baseModel.Pictures;
        }
    }
}
