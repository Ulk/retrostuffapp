﻿using System;

namespace TestsRetroChiasse.Attributes
{
    public class PicturesDirectoryAttribute : Attribute
    {
        private string _directory;

        public PicturesDirectoryAttribute(string directory)
        {
            _directory = directory;
        }
    }
}
