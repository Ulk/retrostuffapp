﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TestsRetroChiasse.Models;

namespace TestsRetroChiasse
{
    public class GenericListPageVM : BaseVM
    {
        #region Properties

        private bool _activityIndicatorIsVisible;
        public bool ActivityIndicatorIsVisible
        {
            get => _activityIndicatorIsVisible;
            set
            {
                _activityIndicatorIsVisible = value;
                NotifyPropertyChanged();
            }
        }

        private bool _gridIsVisible;
        public bool GridIsVisible
        {
            get => _gridIsVisible;
            set
            {
                _gridIsVisible = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<BaseModel> _listItemsSource;
        public ObservableCollection<BaseModel> ListItemsSource
        {
            get => _listItemsSource;
            set
            {
                _listItemsSource = value;
                NotifyPropertyChanged();
            }
        }

        private BaseModel _headerModel;
        public BaseModel HeaderModel
        {
            get => _headerModel;
            set
            {
                _headerModel = value;
                NotifyPropertyChanged();
            }
        }

        public Type CurrentModelType => _listOfStuff?.GetType().GetGenericArguments()[0] ?? typeof(BaseModel);

        #endregion

        private IEnumerable<BaseModel> _listOfStuff;

        public GenericListPageVM(IEnumerable<BaseModel> listOfStuff)
        {
            _listOfStuff = listOfStuff;
            ActivityIndicatorIsVisible = true;
            GridIsVisible = false;
            HeaderModel = (BaseModel)Activator.CreateInstance(CurrentModelType);
        }

        public void LoadDataAsync()
        {
            _ = Task.Run(() =>
            {
                ActivityIndicatorIsVisible = true;
                GridIsVisible = false;

                ListItemsSource = new ObservableCollection<BaseModel>(_listOfStuff);

                ActivityIndicatorIsVisible = false;
                GridIsVisible = true;
            });
        }
    }
}
