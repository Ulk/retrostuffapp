﻿using System;
using TestsRetroChiasse.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestsRetroChiasse.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GenericListPage : ContentPage
    {
        private DataTemplate _ListItemTemplate;
        public DataTemplate ListItemTemplate
        {
            get => _ListItemTemplate;
            set
            {
                _ListItemTemplate = value;
                OnPropertyChanged();
            }
        }

        private Type _currentModelType;
        public Type CurrentModelType
        {
            get => _currentModelType;
            set
            {
                _currentModelType = value;
                OnPropertyChanged();
            }
        }

        public GenericListPage(GenericListPageVM vm)
        {
            InitializeComponent();
            vm.Navigation = Navigation;
            BindingContext = vm;

            CurrentModelType = vm.CurrentModelType;
            ListItemTemplate = new DataTemplate(LoadTemplate);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((GenericListPageVM)BindingContext).LoadDataAsync();
        }

        private object LoadTemplate()
        {
            return new ViewCell
            {
                View = new GenericModelView { ModelType = CurrentModelType }
            };
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // TODO
        }
    }
}