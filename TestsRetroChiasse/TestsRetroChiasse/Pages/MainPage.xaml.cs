﻿using Xamarin.Forms;

namespace TestsRetroChiasse.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageVM { Navigation = Navigation };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}
