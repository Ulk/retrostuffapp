﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using TestsRetroChiasse.Pages;
using TestsRetroChiasse.Services;
using Xamarin.Forms;

namespace TestsRetroChiasse
{
    public class MainPageVM : BaseVM
    {
        #region Properties

        private ICommand _selectFolderButtonCommand;
        public ICommand SelectFolderButtonCommand
        {
            get => _selectFolderButtonCommand;
            set
            {
                _selectFolderButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        private string _selectedFolderLabelText;
        public string SelectedFolderLabelText
        {
            get => _selectedFolderLabelText;
            set
            {
                _selectedFolderLabelText = value;
                NotifyPropertyChanged();
            }
        }

        private ICommand _loadDataButtonCommand;
        public ICommand LoadDataButtonCommand
        {
            get => _loadDataButtonCommand;
            set
            {
                _loadDataButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        private ICommand _purchasesButtonCommand;
        public ICommand PurchasesButtonCommand
        {
            get => _purchasesButtonCommand;
            set
            {
                _purchasesButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        private string _purchasesLabelText;
        public string PurchasesLabelText
        {
            get => _purchasesLabelText;
            set
            {
                _purchasesLabelText = value;
                NotifyPropertyChanged();
            }
        }

        private ICommand _collectiblesButtonCommand;
        public ICommand CollectiblesButtonCommand
        {
            get => _collectiblesButtonCommand;
            set
            {
                _collectiblesButtonCommand = value;
                NotifyPropertyChanged();
            }
        }

        private string _collectiblesLabelText;
        public string CollectiblesLabelText
        {
            get => _collectiblesLabelText;
            set
            {
                _collectiblesLabelText = value;
                NotifyPropertyChanged();
            }
        }

        private string _badRowsLabelText;
        public string BadRowsLabelText
        {
            get => _badRowsLabelText;
            set
            {
                _badRowsLabelText = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        private IDataService _dataService => DependencyService.Get<IDataService>();

        public MainPageVM()
        {
            PurchasesButtonCommand = new Command(async () =>
            {
                await Navigation.PushAsync(
                    new GenericListPage(
                        new GenericListPageVM(DataHolder.Purchases)));
            });

            CollectiblesButtonCommand = new Command(async () =>
            {
                await Navigation.PushAsync(
                    new GenericListPage(
                        new GenericListPageVM(DataHolder.Collectibles)));
            });

            SelectFolderButtonCommand = new Command(async () =>
            {
                SelectedFolderLabelText = await _dataService.PickFolder();
            });

            LoadDataButtonCommand = new Command(async () =>
            {
                await LoadData();
            });

            Init();
        }

        private async void Init()
        {
            // TODO: Display some kind of loading
            SelectedFolderLabelText = await _dataService.TryAutoLoadFolder();
            await LoadData();
        }

        private async Task LoadData()
        {
            await _dataService.LoadData();
            PurchasesLabelText = $"{DataHolder.Purchases?.Count}";
            CollectiblesLabelText = $"{DataHolder.Collectibles?.Count}";
            BadRowsLabelText = $"Bad rows: {DataHolder.BadRows.Count}";

            foreach(var row in DataHolder.BadRows)
            {
                BadRowsLabelText += $"\n{row}";
            }
        }
    }
}
