﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestsRetroChiasse
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PurchaseListItemView : ContentView
    {
        public PurchaseListItemView()
        {
            InitializeComponent();
        }
    }
}