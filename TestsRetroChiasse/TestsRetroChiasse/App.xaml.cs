﻿using TestsRetroChiasse.Pages;
using Xamarin.Forms;

namespace TestsRetroChiasse
{
    public partial class App : Application
    {
        private NavigationPage _navigationPage;

        public App()
        {
            Xamarin.Forms.DataGrid.DataGridComponent.Init();
            InitializeComponent();
            MainPage = _navigationPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
