﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Linq;
using Xamarin.Forms;

namespace TestsRetroChiasse.Views
{
    public class GenericModelView : Grid
    {
        public static readonly BindableProperty ModelTypeProperty = BindableProperty.Create(
            nameof(ModelType),
            typeof(Type),
            typeof(GenericModelView),
            propertyChanged: OnModelTypeChanged);

        public Type ModelType
        {
            get => (Type)GetValue(ModelTypeProperty);
            set => SetValue(ModelTypeProperty, value);
        }

        private static void OnModelTypeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var modelGenericView = (GenericModelView)bindable;
            var modelType = (Type)newValue;

            // Reset the view
            modelGenericView.ColumnDefinitions.Clear();
            modelGenericView.Children.Clear();

            int currentColumn = 0;

            foreach (var propInfo in modelType.GetProperties())
            {
                var nameAttribute = propInfo.GetCustomAttributes(true).FirstOrDefault(attribute => attribute is NameAttribute);

                if (nameAttribute != null)
                {
                    var labello = new Label();
                    labello.SetBinding(Label.TextProperty, new Binding(propInfo.Name));

                    modelGenericView.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                    modelGenericView.Children.Add(labello, currentColumn, 0);
                    currentColumn++;
                }
            }
        }
    }
}
