﻿using CsvHelper.Configuration.Attributes;
using System.Linq;
using TestsRetroChiasse.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestsRetroChiasse
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GenericListItem : ContentView
    {
        public GenericListItem()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext is BaseModel)
            {
                int currentColumn = 0;

                foreach (var propInfo in BindingContext.GetType().GetProperties())
                {
                    var nameAttribute = propInfo.GetCustomAttributes(true).FirstOrDefault(attribute => attribute is NameAttribute);

                    if (nameAttribute != null)
                    {
                        Agrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                        Agrid.Children.Add(new Label() { Text = propInfo.GetValue(BindingContext) as string }, currentColumn, 0);
                        currentColumn++;
                    }
                }
            }
        }
    }
}