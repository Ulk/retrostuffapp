﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestsRetroChiasse.Models;
using Xamarin.Essentials;

namespace TestsRetroChiasse.Services
{
    public interface IDataService
    {
        Task<string> TryAutoLoadFolder();
        Task<string> PickFolder();
        Task LoadData();
    }

    public static class DataHolder
    {
        public static List<string> BadRows = new();
        public static List<PurchaseModel> Purchases;
        public static List<CollectibleModel> Collectibles;
    }

    public static class DataHelper
    {
        private const string _currentFolderKey = "currentFolder";
        private const string _currentFolderTokenKey = "currentFolderToken";

        public static void SaveCurrentFolderToPreferences(string path)
        {
            Preferences.Set(_currentFolderKey, path);
        }

        public static string GetCurrentFolderFromPreferences()
        {
            return Preferences.Get(_currentFolderKey, defaultValue: null);
        }

        public static void SaveCurrentFolderTokenToPreferences(string token)
        {
            Preferences.Set(_currentFolderTokenKey, token);
        }

        public static string GetCurrentFolderTokenFromPreferences()
        {
            return Preferences.Get(_currentFolderTokenKey, defaultValue: null);
        }
    }
}
