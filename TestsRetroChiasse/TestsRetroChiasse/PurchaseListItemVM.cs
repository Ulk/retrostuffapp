﻿using System;

namespace TestsRetroChiasse
{
    public class PurchaseListItemVM : BaseVM
    {
        private string _idCustom;
        public string IdCustom
        {
            get => _idCustom;
            set
            {
                _idCustom = value;
                NotifyPropertyChanged();
            }
        }

        private string _formattedDate;
        public string FormattedDate
        {
            get => _formattedDate;
            set
            {
                _formattedDate = value;
                NotifyPropertyChanged();
            }
        }

        private string _price;
        public string Price
        {
            get => _price;
            set
            {
                _price = value;
                NotifyPropertyChanged();
            }
        }

        private string _buyingPlace;
        public string BuyingPlace
        {
            get => _buyingPlace;
            set
            {
                _buyingPlace = value;
                NotifyPropertyChanged();
            }
        }

        private string _seller;
        public string Seller
        {
            get => _seller;
            set
            {
                _seller = value;
                NotifyPropertyChanged();
            }
        }

        private string _observations;
        public string Observations
        {
            get => _observations;
            set
            {
                _observations = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime _date;
    }
}
