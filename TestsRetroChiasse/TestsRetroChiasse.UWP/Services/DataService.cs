﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using TestsRetroChiasse.Models;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.Storage.Pickers;
using Xamarin.Forms;

[assembly: Dependency(typeof(TestsRetroChiasse.Services.UWP.DataService))]
namespace TestsRetroChiasse.Services.UWP
{
    public class DataService : IDataService
    {
        private StorageFolder _currentFolder;

        public async Task<string> TryAutoLoadFolder()
        {
            var savedFolderPath = DataHelper.GetCurrentFolderFromPreferences();
            var savedFolderToken = DataHelper.GetCurrentFolderTokenFromPreferences();

            if (!string.IsNullOrWhiteSpace(savedFolderPath)
                && !string.IsNullOrWhiteSpace(savedFolderToken)
                && StorageApplicationPermissions.FutureAccessList.ContainsItem(savedFolderToken))
            {
                _currentFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(savedFolderToken);
                if (_currentFolder != null)
                {
                    return savedFolderPath;
                }
            }
            return null;
        }

        public async Task<string> PickFolder()
        {
            var openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.Desktop;
            openPicker.FileTypeFilter.Add(".tsv");

            _currentFolder = await openPicker.PickSingleFolderAsync();
            if (_currentFolder != null)
            {
                DataHelper.SaveCurrentFolderToPreferences(_currentFolder.Path);
                // Mandatory on UWP
                string token = Guid.NewGuid().ToString();
                StorageApplicationPermissions.FutureAccessList.AddOrReplace(token, _currentFolder);
                DataHelper.SaveCurrentFolderTokenToPreferences(token);
            }
            else
            {
                // Cancelled
            }
            
            return _currentFolder?.Path;
        }

        public async Task LoadData()
        {
            if (_currentFolder != null)
            {
                try
                {
                    DataHolder.BadRows.Clear();

                    var purchasesFile = await _currentFolder.GetFileAsync("purchases.tsv");
                    DataHolder.Purchases = await GetDataFromCsvFile<PurchaseModel>(purchasesFile);
                
                    var collectiblessFile = await _currentFolder.GetFileAsync("collectibles.tsv");
                    DataHolder.Collectibles = await GetDataFromCsvFile<CollectibleModel>(collectiblessFile);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"LoadData exception: {e}");
                }
            }
        }

        //public async Task<string> PickFolderAndLoadData()
        //{
        //    var openPicker = new FolderPicker();
        //    openPicker.ViewMode = PickerViewMode.List;
        //    openPicker.SuggestedStartLocation = PickerLocationId.Desktop;
        //    openPicker.FileTypeFilter.Add(".tsv");

        //    var folder = await openPicker.PickSingleFolderAsync();
        //    if (folder != null)
        //    {
        //        // Purchases
        //        var purchasesFile = await folder.GetFileAsync("Purchases.tsv");
        //        DataHolder.Purchases = await GetDataFromCsvFile<PurchaseModel>(purchasesFile);
        //    }
        //    else
        //    {
        //        // Cancelled
        //    }

        //    return folder.Path;
        //}

        private async Task<List<T>> GetDataFromCsvFile<T>(StorageFile file)
        {
            if (file != null)
            {
                using (var stream = await file.OpenStreamForReadAsync())
                using (var reader = new StreamReader(stream))
                {
                    if (reader != null)
                    {
                        using var csv = new CsvReader(
                            reader,
                            new CsvConfiguration(CultureInfo.CurrentCulture)
                            {
                                Delimiter = "\t",
                                HeaderValidated = null,
                                MissingFieldFound = null,
                                Quote = '§',
                                Mode = CsvMode.Escape,
                                ShouldQuote = (args) => false,
                                BadDataFound = (args) => {
                                    DataHolder.BadRows.Add(args.RawRecord);
                                    Console.WriteLine($"BAD DATA FOUND: {args.RawRecord}");
                                }
                            });
                        return new List<T>(csv.GetRecords<T>());
                    }
                }
            }
            else
            {
                // Cancelled
            }

            return new List<T>();
        }
    }
}
